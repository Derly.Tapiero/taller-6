// crear un objeto (variable compuestas) para representar el visor
//L representa a la biblioteca leaflet
let miMapa = L.map('mapid');

//Determinar la vista inicial
miMapa.setView([4.497077,-74.104117], 15);
let miProveedor= L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png');
miProveedor.addTo(miMapa);


//Crear un objeto marcador
let miMarcador = L.marker([4.497077,-74.104117]);
miMarcador.addTo(miMapa);

let miMarcador2 = L.marker([4.497168,-74.103924]);
miMarcador2.addTo(miMapa);


//JSON
let circle = L.circle([4.497077,-74.104117], {
    color: 'red',
    fillColor: 'red',
    fillOpacity: 0.5,
    radius: 100
});

circle.addTo(miMapa);

let polygon = L.polygon([
    [ 4.489975051873706,
       -74.09847021102905],
       
      [4.489156814318392,
        -74.09999370574951],

      [ 4.488306488043731,
        -74.09961819648743],

      [4.488515058731086,
        -74.09878671169281],

      [4.488274400240373,
        -74.09836292266846,],

      [4.488798500851722,
        -74.098100066185],

      [4.489975051873706,
        -74.09847021102905]
   
]).addTo(miMapa);
